# Maintainer: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
# Contributor: Balló György <ballogyor+arch at gmail dot com>

pkgname=gnome-connections
pkgver=44rc
pkgrel=1
pkgdesc='Remote desktop client for the GNOME desktop environment'
arch=('x86_64')
url='https://gitlab.gnome.org/GNOME/connections'
license=('GPL3')
depends=(
  freerdp
  gtk-vnc
  libhandy
  libsecret
)
makedepends=(
  appstream-glib
  git
  gobject-introspection
  itstool
  meson
  vala
)
groups=('gnome-extra')
options=('!emptydirs')
_commit=72ff74d844792e56c6548c2630cb7f721bad006d  # tags/44.rc^0
source=("git+https://gitlab.gnome.org/GNOME/connections.git#commit=$_commit")
sha256sums=('SKIP')

pkgver() {
  cd connections
  git describe --tags | sed -r 's/\.([a-z])/\1/;s/([a-z])\./\1/;s/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd connections
  # Don't use legacy path for metainfo file
  sed -i "s/datadir'), 'appdata/datadir'), 'metainfo/" data/meson.build
  # Add categories to desktop file
  sed -i 's/Categories=GTK;/Categories=GNOME;GTK;Utility;RemoteAccess;Network;/' \
    data/org.gnome.Connections.desktop.in

}

build() {
  arch-meson connections build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"

  # Remove unneeded development files
  rm -r "$pkgdir"/usr/include/gnome-connections/gtk-frdp \
        "$pkgdir"/usr/lib/gnome-connections/{girepository-1.0,pkgconfig} \
        "$pkgdir"/usr/share/gnome-connections/{gir-1.0,vapi}
}
